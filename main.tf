module "networking" {
  source     = "./modules/networking"
  cidr_block = var.cidr_block
  subnets    = var.subnets
  eip_name   = var.eip_name
  nat_name   = var.nat_name
  rt_name    = var.rt_name
}